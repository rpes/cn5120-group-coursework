package clientSide;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

public class ChatRoomRefresher extends Thread{
	private JPanel jPanel;
	private JScrollPane jScrollPane;

	public ChatRoomRefresher(JPanel panel){//, JScrollPane scroll){
		jPanel = panel;
		//jScrollPane = scroll;
	}
	

	public void run(){
		while(true){
			jPanel.repaint();
			jPanel.setVisible(false);
			jPanel.setVisible(true);
			
			try{
				Thread.sleep(250);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	
	public void update(){
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	while(true){
		    	jPanel.repaint();
		    	}
		    }
		  });
	}
}
