package clientSide;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Register extends JFrame {

	private JPanel contentPane;
	private JTextField CreateUsernameTextField;
	private JLabel CreateUsernameLabel;
	private JLabel CreatePasswordLabel;
	private JLabel ConfirmPasswordLabel;
	private JPasswordField CreatePasswordTextField;
	private JPasswordField ConfirmPasswordTextField;
	private JFrame register;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register frame = new Register();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Register() {
		register = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 453, 298);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegister = new JLabel("Register");
		lblRegister.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblRegister.setBounds(148, 24, 144, 58);
		contentPane.add(lblRegister);
		
		CreateUsernameTextField = new JTextField();
		CreateUsernameTextField.setBounds(113, 93, 253, 20);
		contentPane.add(CreateUsernameTextField);
		CreateUsernameTextField.setColumns(10);
		
		JButton CreateButton = new JButton("Create");
		CreateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFrame welcome = new JFrame();
				JOptionPane.showMessageDialog(welcome, "Thank you for registering. Please login to advance");
				Login login = new Login();
				login.setVisible(true);
				register.dispose();
				
			}
		});
		
		CreatePasswordTextField = new JPasswordField();
		CreatePasswordTextField.setBounds(113, 136, 253, 20);
		contentPane.add(CreatePasswordTextField);
		
		ConfirmPasswordTextField = new JPasswordField();
		ConfirmPasswordTextField.setBounds(113, 176, 253, 20);
		contentPane.add(ConfirmPasswordTextField);
		
		
		CreateButton.setBounds(203, 225, 89, 23);
		contentPane.add(CreateButton);
		
		JButton BackButton = new JButton("Back ");
		BackButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login bktologin = new Login();
				bktologin.setVisible(true);
				register.dispose();
			}
		});
		BackButton.setBounds(338, 225, 89, 23);
		contentPane.add(BackButton);
		
		CreateUsernameLabel = new JLabel("Create Username");
		CreateUsernameLabel.setBounds(10, 97, 93, 14);
		contentPane.add(CreateUsernameLabel);
		
		CreatePasswordLabel = new JLabel("Create Password");
		CreatePasswordLabel.setBounds(10, 139, 82, 14);
		contentPane.add(CreatePasswordLabel);
		
		ConfirmPasswordLabel = new JLabel("Confirm Password");
		ConfirmPasswordLabel.setBounds(10, 178, 93, 17);
		contentPane.add(ConfirmPasswordLabel);
	}

}
