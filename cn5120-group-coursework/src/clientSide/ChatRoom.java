package clientSide;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.awt.GridLayout;

public class ChatRoom {

	private JFrame frame;
	private PrintWriter clientToServer;
	private JPanel DisplayMessagesText;
	private static List FriendsList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatRoom window = new ChatRoom();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ChatRoom() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 643, 455);
		frame.setSize(643, 455);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextArea sendMessageText = new JTextArea();
		JScrollPane textBox = new JScrollPane(sendMessageText);
		textBox.setPreferredSize(new Dimension(462, 111));
		textBox.setBounds(10, 294, 462, 111);
		sendMessageText.setBounds(10, 294, 462, 111);
		frame.getContentPane().add(textBox);
		
		JButton SendButton = new JButton("Send");
		SendButton.setBounds(482, 294, 135, 111);
		frame.getContentPane().add(SendButton);
		SendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					if(!sendMessageText.getText().equals("")){
					clientToServer.println(sendMessageText.getText());
					}
					sendMessageText.setText(null);
				}catch(Exception exception){
					//do nothing prevents nulls from being sent
				}
			}
		});
		
		DisplayMessagesText = new JPanel(new GridLayout());
		JScrollPane Scroll = new JScrollPane(DisplayMessagesText);
		DisplayMessagesText.setLayout(new BoxLayout(DisplayMessagesText, BoxLayout.Y_AXIS));
		Scroll.setPreferredSize(new Dimension(462,214));
		Scroll.setBounds(10, 69, 462, 214);
		DisplayMessagesText.setBounds(10, 69, 462, 214);
		frame.getContentPane().add(Scroll);
		
		FriendsList = new List();
		FriendsList.setBounds(480, 69, 137, 214);
		frame.getContentPane().add(FriendsList);
		
		JLabel lblChatRoom = new JLabel("Chat Room");
		lblChatRoom.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblChatRoom.setBounds(220, 11, 143, 47);
		frame.getContentPane().add(lblChatRoom);
		
		updater();
	}

	private void updater(){
		ChatRoomRefresher refresh = new ChatRoomRefresher(DisplayMessagesText);
		refresh.start();
	}
	
	public List getList(){
		return FriendsList;
	}
	
	public JPanel getChatBox(){
		return DisplayMessagesText;
	}
	
	public JFrame getFrame(){
		return frame;
	}
	
	public void addPrintWriter(PrintWriter pw){
		clientToServer = pw;
	}
}
