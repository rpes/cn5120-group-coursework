package clientSide;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends JFrame {
	private JTextField UsernameTextField;
	private JPasswordField PasswordText1;
	private JFrame login;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		login = this;
		setBounds(100, 100, 398, 250);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		UsernameTextField = new JTextField();
		UsernameTextField.setBounds(105, 82, 223, 20);
		getContentPane().add(UsernameTextField);
		UsernameTextField.setColumns(10);
		
		PasswordText1 = new JPasswordField();
		PasswordText1.setBounds(105, 113, 223, 20);
		getContentPane().add(PasswordText1);
		
		JLabel UsernameLabel1 = new JLabel("Username");
		UsernameLabel1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		UsernameLabel1.setBounds(28, 83, 84, 14);
		getContentPane().add(UsernameLabel1);
		
		JLabel PasswordLabel1 = new JLabel("Password");
		PasswordLabel1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		PasswordLabel1.setBounds(28, 119, 67, 14);
		getContentPane().add(PasswordLabel1);
		
		JLabel SignRegLabel = new JLabel("Sign In/Register");
		SignRegLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		SignRegLabel.setBounds(110, 21, 157, 34);
		getContentPane().add(SignRegLabel);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChatRoom cr = new ChatRoom();
				cr.getFrame().setVisible(true);
				login.dispose();
			}
		});
		btnLogin.setBounds(145, 177, 89, 23);
		getContentPane().add(btnLogin);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Register reg = new Register();
				reg.setVisible(true);
				login.dispose();
			}
		});
		btnRegister.setBounds(257, 177, 89, 23);
		getContentPane().add(btnRegister);

	}
}
