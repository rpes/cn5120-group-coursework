package clientSide;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Client {
	private Scanner scan = new Scanner(System.in);
	private Scanner scan2 = new Scanner(System.in);
	private static ChatRoom chat;

	public static void main(String[] args) {
		Client e2 = new Client();
		e2.inputConnection();
		
	}
	
	public void inputConnection(){
		boolean doneport = false;
		boolean doneip = false;
		int portNumber = 0;
		String ip = "";
		while(doneport != true){
			System.out.println("Please enter the port number");
			scan = new Scanner(System.in);
			try{
				portNumber = scan.nextInt();
				doneport = true;
			} catch (InputMismatchException e){
				System.out.println("Please enter an integer value");
			}
		}
		while(doneip != true){
			System.out.println("Please enter the IP Address");
			scan2 = new Scanner(System.in);
				ip = scan2.nextLine();
				doneip = true;
		}
		testConnection(ip, portNumber);
	}
	
	public void testConnection(String ip, int port){
		try {
			Socket s = new Socket(ip,port);
			connectEcho(ip,port);
		} catch(UnknownHostException e){
			System.out.println("Unable to connect to server! It may be offline");
			inputConnection();
		} catch (IOException e) {
			System.out.println("Please enter valid IP and Port");
			inputConnection();
		} catch (IllegalArgumentException e){
			System.out.println("Please enter a valid Port number");
			inputConnection();
		}
	}

	public void connectEcho(String i, int p){
		//String IP = "10.64.21.130";
		//int port = 10007;
		String IP = i;
		int port = p;
		Socket s = null;
		try {
			s = new Socket(IP, port);
			chat = new ChatRoom();
			BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
			BufferedReader sIn = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter sOut = new PrintWriter(s.getOutputStream(), true);
			System.out.println("Welcome to the echo Client Server service, if you wish to exit, please enter 'Bye.' ");
			System.out.println("Note, entering nothing will result in a reponse of nothing!");
			chat.getFrame().setVisible(true);
			chat.addPrintWriter(sOut);
			
			ClientServerReplyThread serverHandler = new ClientServerReplyThread(sIn, chat);
			serverHandler.start();
			
			String input = userInput.readLine();
			while(true){
				if(!input.equals("")){
				sOut.println(input);
				}
				//String in = sIn.readLine();
				//if(in != null){
				//System.out.println(in);
				input = userInput.readLine();
				//}
				//else{
				//	break;
				//}
			}
			
		}catch (UnknownHostException e){
			System.out.println("Sorry! There was a problem with finding the host! - "+e.toString());
		}catch (IOException e) {
			System.out.println("Sorry! There was a problem regarding I/O! - "+e.toString());
			System.out.println("Closing Program");
		}finally{
			try {
				s.close();
				scan.close();
				scan2.close();
				System.out.println("Thanks for using our services, goodbye.");
			} catch (IOException e) {
				System.out.println("Oh no! There was a problem with closing the Socket! - "+e.toString());
				System.out.println("Closing Program");
			} 
		}
		
	}
	
}