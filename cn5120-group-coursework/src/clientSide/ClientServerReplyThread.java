package clientSide;

import java.io.BufferedReader;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class ClientServerReplyThread extends Thread {
	private BufferedReader br;
	private ChatRoom chat;

	public ClientServerReplyThread(BufferedReader b, ChatRoom cr){
		br = b;
		chat = cr;
	}
	
	public void run(){
		while (true){
			try {
				String in = br.readLine();
				JLabel message = new JLabel(in);
				chat.getChatBox().add(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
