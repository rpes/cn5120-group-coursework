package serverSide;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class MainServerMultiCom extends Thread {
	private Socket echoClientSocket;
	private PrintWriter output;
	private static Broadcast broadcast;
	private int userNo;
	
	public MainServerMultiCom(Socket s, int userNumber, Broadcast b){
		echoClientSocket = s;
		userNo = userNumber;
		broadcast = b;
		broadcast.addClient(s);
	}
	
	public void run(){
		try {
			output = new PrintWriter(echoClientSocket.getOutputStream(), true);
			BufferedReader input = new BufferedReader(new InputStreamReader(echoClientSocket.getInputStream()));
			output.println("Welcome to the Chat room User "+userNo);
			readInputs(input,output);
		} catch (IOException e) {
			System.out.println("There was an error with I/O during connection with client! - "+e.toString());
		}
	
	}
	
	public void readInputs(BufferedReader in, PrintWriter out) throws IOException{
		String userInput;
		
		while((userInput = in.readLine()) != null){
				System.out.println("Server- " + userInput);
				broadcast.sendToAll("User "+userNo+": "+userInput);
		}
		out.close();
		in.close();
	}
	
	/*public void sendMessage(String s){
		if(echoClientSocket.isConnected()){
			output.println(s);
		}
	}*/
}
