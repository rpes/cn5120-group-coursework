package serverSide;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class Broadcast {
	private static ArrayList<Socket> clients = new ArrayList<Socket>();
	
	public synchronized void sendToAll(String s) throws IOException{
		for (Socket c : clients){
			if (c.isConnected()){
				PrintWriter sender = new PrintWriter(c.getOutputStream(), true);
				sender.println(s);
			}
			else {
				clients.remove(c);
			}
		}
	}
	
	public void addClient(Socket s){
		clients.add(s);
	}
}
