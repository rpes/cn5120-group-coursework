package serverSide;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MainServer {
	//private int port = 13337;
	private int number = 0;
	private static Broadcast broadcast;
	private Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		MainServer echo = new MainServer();
		echo.connect();
	}
	public void connect(){
		boolean done = false;
		int port = 0;
		while(done != true){
			System.out.println("Please enter the port number");
			scan = new Scanner(System.in);
			try{
				port = scan.nextInt();
				done = true;
			} catch (InputMismatchException e){
				System.out.println("Please enter an integer value");
			}
		}
		startConnection(port);
	}
	
	public void startConnection(int n){
		ServerSocket echoSocket = null;
		Socket echoClientSocket = null;
		broadcast = new Broadcast();
		try {
			echoSocket = new ServerSocket(n);
			while(true){
				System.out.println("Waiting for connection...");
				echoClientSocket = echoSocket.accept();
				System.out.println("Connection with "+echoClientSocket.getInetAddress()+" has been successful");
				System.out.println("Waiting for client input(s)");
				MainServerMultiCom client = new MainServerMultiCom(echoClientSocket, number, broadcast);
				number++;
				client.start();
			}
			
		} catch (IOException e) {
			System.out.println("Problem regardin I/O! - "+e.toString());
		} catch (IllegalArgumentException e){
			System.out.println("Please enter a valid port num"
					+ "ber");
			connect();
		} finally{
			scan.close();
		}
	}

}